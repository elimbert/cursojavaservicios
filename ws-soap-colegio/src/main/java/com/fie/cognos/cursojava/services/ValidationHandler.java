package com.fie.cognos.cursojava.services;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class ValidationHandler implements SOAPHandler<SOAPMessageContext> {

	public void close(MessageContext arg0) {
		System.out.println("Entro a Close");
		// TODO Auto-generated method stub
		
	}

	public boolean handleFault(SOAPMessageContext arg0) {
		System.out.println("Entro a HandleFault");
		// TODO Auto-generated method stub
		return false;
	}

	public boolean handleMessage(SOAPMessageContext arg0) {
		System.out.println("Entro a HandleMessage");
		mostrarMensaje(arg0);
		// TODO Auto-generated method stub
		//return true;
		if (validarMensaje(arg0)) {
			return true;
		}
		return false;
	}

	public Set<QName> getHeaders() {
		System.out.println("getHeaders");
		// TODO Auto-generated method stub
		return null;
	}

	public void mostrarMensaje(SOAPMessageContext context) {

		try {
			boolean isOutbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
			if(isOutbound)
				System.out.println("Mensaje de Salida...");
			else
				System.out.println("Mensaje de Entrada...");
			
			SOAPMessage soapMessage = context.getMessage();
			soapMessage.writeTo(System.out);
		} catch (SOAPException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean validarMensaje(SOAPMessageContext context) {
		boolean isOutbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if(!isOutbound) {
			System.out.println("Mensajes de Entradad");
			HttpServletRequest request = (HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST);
			System.out.println("IP ======>" + request.getRemoteAddr());
			System.out.println("HOST ======>" + request.getRemoteHost());
			System.out.println("USARIO ======>" + request.getHeader("usuario"));
			
			return true;
		}
		return false;
	}
	
}
