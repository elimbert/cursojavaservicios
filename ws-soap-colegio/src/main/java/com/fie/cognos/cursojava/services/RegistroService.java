package com.fie.cognos.cursojava.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import com.fie.cognos.cursojava.entities.Persona;

@WebService
@SOAPBinding(style = Style.RPC, use=Use.LITERAL)
public class RegistroService {
	
	@WebMethod
	@WebResult(name = "persona")
	public String registrarPersona(Persona persona) {
		return "Registrado: " + persona.getNombre() + " " + persona.getPaterno();
	}
	
	@WebMethod
	public Persona recuperarPersona(@WebParam(name = "ci") String ci) {
		Persona persona = new Persona();
		persona.setCi(ci);
		persona.setNombre("Pablo");
		persona.setPaterno("Villamil");
		
		return persona;
	}
}
