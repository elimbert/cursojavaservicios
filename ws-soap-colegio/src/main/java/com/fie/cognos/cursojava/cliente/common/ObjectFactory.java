
package com.fie.cognos.cursojava.cliente.common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fie.cognos.cursojava.cliente.common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CargarPersona_QNAME = new QName("http://services.soap.cursojava.cognos.fie.com/", "cargarPersona");
    private final static QName _CargarPersonaResponse_QNAME = new QName("http://services.soap.cursojava.cognos.fie.com/", "cargarPersonaResponse");
    private final static QName _Saludar_QNAME = new QName("http://services.soap.cursojava.cognos.fie.com/", "saludar");
    private final static QName _ListarPeronasResponse_QNAME = new QName("http://services.soap.cursojava.cognos.fie.com/", "listarPeronasResponse");
    private final static QName _SaludarResponse_QNAME = new QName("http://services.soap.cursojava.cognos.fie.com/", "saludarResponse");
    private final static QName _Exception_QNAME = new QName("http://services.soap.cursojava.cognos.fie.com/", "Exception");
    private final static QName _ListarPeronas_QNAME = new QName("http://services.soap.cursojava.cognos.fie.com/", "listarPeronas");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fie.cognos.cursojava.cliente.common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CargarPersona }
     * 
     */
    public CargarPersona createCargarPersona() {
        return new CargarPersona();
    }

    /**
     * Create an instance of {@link CargarPersonaResponse }
     * 
     */
    public CargarPersonaResponse createCargarPersonaResponse() {
        return new CargarPersonaResponse();
    }

    /**
     * Create an instance of {@link Saludar }
     * 
     */
    public Saludar createSaludar() {
        return new Saludar();
    }

    /**
     * Create an instance of {@link ListarPeronasResponse }
     * 
     */
    public ListarPeronasResponse createListarPeronasResponse() {
        return new ListarPeronasResponse();
    }

    /**
     * Create an instance of {@link SaludarResponse }
     * 
     */
    public SaludarResponse createSaludarResponse() {
        return new SaludarResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ListarPeronas }
     * 
     */
    public ListarPeronas createListarPeronas() {
        return new ListarPeronas();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersona }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.soap.cursojava.cognos.fie.com/", name = "cargarPersona")
    public JAXBElement<CargarPersona> createCargarPersona(CargarPersona value) {
        return new JAXBElement<CargarPersona>(_CargarPersona_QNAME, CargarPersona.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersonaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.soap.cursojava.cognos.fie.com/", name = "cargarPersonaResponse")
    public JAXBElement<CargarPersonaResponse> createCargarPersonaResponse(CargarPersonaResponse value) {
        return new JAXBElement<CargarPersonaResponse>(_CargarPersonaResponse_QNAME, CargarPersonaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Saludar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.soap.cursojava.cognos.fie.com/", name = "saludar")
    public JAXBElement<Saludar> createSaludar(Saludar value) {
        return new JAXBElement<Saludar>(_Saludar_QNAME, Saludar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPeronasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.soap.cursojava.cognos.fie.com/", name = "listarPeronasResponse")
    public JAXBElement<ListarPeronasResponse> createListarPeronasResponse(ListarPeronasResponse value) {
        return new JAXBElement<ListarPeronasResponse>(_ListarPeronasResponse_QNAME, ListarPeronasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaludarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.soap.cursojava.cognos.fie.com/", name = "saludarResponse")
    public JAXBElement<SaludarResponse> createSaludarResponse(SaludarResponse value) {
        return new JAXBElement<SaludarResponse>(_SaludarResponse_QNAME, SaludarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.soap.cursojava.cognos.fie.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPeronas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.soap.cursojava.cognos.fie.com/", name = "listarPeronas")
    public JAXBElement<ListarPeronas> createListarPeronas(ListarPeronas value) {
        return new JAXBElement<ListarPeronas>(_ListarPeronas_QNAME, ListarPeronas.class, null, value);
    }

}
