package com.fie.cognos.cursojava.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import com.fie.cognos.cursojava.entities.Persona;


@WebService
@HandlerChain(file = "handler-chain.xml")
@SOAPBinding(style = Style.DOCUMENT, use=Use.LITERAL)
public class CommonService {
	
	@Resource
	WebServiceContext wsc;
	
	@WebMethod
	public String autenticar() throws Exception {
		MessageContext messageContext = wsc.getMessageContext();
		Map<String, Object> headers = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
		List<String> usuario = (List<String>) headers.get("usuario");
		List<String> password = (List<String>) headers.get("password");
		if(usuario != null && password != null) {
			if (usuario.get(0).equals("ariel") && password.get(0).equals("patino")) {
				return "Bienvenido ariel";
			}
		}
		//return "Error de autenticacion ";
		throw new Exception("Error de autenticacion throw Excepciones");
		
	}
	
	
	@WebMethod
	public String cargarPersonas(List<Persona> lista) {
		return "Datos cargado";
	}
	
	@WebMethod
	public List<Persona> recuperarPersonas() {		
		
		
		List<Persona> personasList = new ArrayList<Persona>();
		
		Persona persona = new Persona();
		persona.setCi("1");
		persona.setNombre("Pablo");
		persona.setPaterno("Villamil");
		personasList.add(persona);
		
		Persona persona1 = new Persona();
		persona1.setCi("2");
		persona1.setNombre("Juan");
		persona1.setPaterno("Perez");
		personasList.add(persona1);

		Persona persona2 = new Persona();
		persona2.setCi("3");
		persona2.setNombre("Pedro");
		persona2.setPaterno("Peredo");
		personasList.add(persona2);
		
		return personasList;
		
	}
	

}
