package com.fie.cognos.cursojava.entities;

public class Persona {
	private String ci;
	private String nombre;
	private String paterno;
	private String materno;
	private String edad;
	
	
	
	public Persona() {
		super();
	}

	public Persona(String ci, String nombre, String paterno, String materno, String edad) {
		super();
		this.ci = ci;
		this.nombre = nombre;
		this.paterno = paterno;
		this.materno = materno;
		this.edad = edad;
	}
	
	public String getCi() {
		return ci;
	}
	public void setCi(String ci) {
		this.ci = ci;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
}
