package com.fie.cognos.cursojava.services;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

@WebService
@SOAPBinding(style = Style.DOCUMENT, use=Use.LITERAL)
public class HolaMundoServices {

	@WebMethod
	public String saludar() {
		return "Hola Mundo!!!";
	}
	
	@WebMethod
	public int sumar (int a, int b ) {
		return a + b;
	}
}
